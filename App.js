import { AppRegistry, StyleSheet, Text, View, Image,Dimensions,Platform,StatusBar,SafeAreaView ,NetInfo,Animated} from 'react-native';
import React, { Component } from 'react';
import { createStackNavigator, createAppContainer } from "react-navigation";
import { Root} from 'native-base';
import {FeedScreen} from './screens/Feedscreen/feedscreen'
import {ViewfeedScreen} from './screens/Viewfeedscreen/viewfeedscreen'
import Toast, {DURATION} from 'react-native-easy-toast'
import { Constants,AppLoading, Asset, Font, SplashScreen } from 'expo';
import Icon from '@expo/vector-icons/MaterialCommunityIcons'
const MyStackNavigator = createStackNavigator({

  FeedScreen:{
    screen:FeedScreen
  },
  ViewfeedScreen:{
    screen:ViewfeedScreen
  }
    },

    {
        initialRouteName: 'FeedScreen',
        drawerWidth: 300,
        headerMode: 'none'
    }
);


const MyApp = createAppContainer(MyStackNavigator);
console.disableYellowBox = true;
export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = { loading: true,isLoadingComplete: false,
          splashAnimation: new Animated.Value(0),
          splashAnimationComplete: false, };
    }

    componentDidMount() {
      SplashScreen.preventAutoHide();
      this._loadAsync();
        NetInfo.isConnected.addEventListener(
            'connectionChange',
            this._handleConnectivityChange
    
        );
       
        NetInfo.isConnected.fetch().done((isConnected) => {
    
          if(isConnected == true)
          {
            // this.setState({connection_Status : "Online",isConenction:true})
            this.setState({connection_Status : "connected",isConenction:true})
          }
          else
          {
            // this.setState({connection_Status : "Offline",isConenction:false})
            this.setState({connection_Status : "not connected",isConenction:false})
          }
    
        });
      }
      _loadAsync = async () => {
        try {
          await this._loadResourcesAsync();
          await Font.loadAsync({
            'Roboto': require('./assets/Fonts/Roboto.ttf'),
            'Roboto_medium': require('./assets/Fonts/Roboto_medium.ttf'),
          });
        } catch (e) {
          this._handleLoadingError(e);
        } finally {
          this._handleFinishLoading();
        }
      };
      componentWillUnmount() {
    
        NetInfo.isConnected.removeEventListener(
            'connectionChange',
            this._handleConnectivityChange
    
        );
    
      }
      _handleConnectivityChange = (isConnected) => {
    
        if(isConnected == true)
          {
            // this.setState({connection_Status : "Online",isConenction:true})
            this.setState({connection_Status : "connected",isConenction:true})
          }
          else
          {
            // this.setState({connection_Status : "Offline",isConenction:false})
            this.setState({connection_Status : "not connected",isConenction:false})
          }
      };
      setNavigationColor = async(color) => {
        await changeNavigationBarColor(color);
      };
      example = async () => {
        try{
            const response = await changeNavigationBarColor('#3C5995');
            console.log(response)// {success: true}
        }catch(e){
            console.log(e)// {success: false}
        }
    
    };
    toastready=()=>{
      this.refs.toast.show('No Internet Connection', 2000);
    }
      render() {
        if (!this.state.isLoadingComplete) {
          return <View />;
        }
        if(!this.state.isConenction){
          return (
              <SafeAreaView style={styles.safeArea}>
              <Root><MyApp /></Root>{this._maybeRenderLoadingImage()}
              <View style={styles.offlineContainer}><Text style={styles.offlineText}>No Internet Connection</Text></View>
              </SafeAreaView>
          )
        }
        return (<SafeAreaView style={styles.safeArea}><Root><MyApp /></Root>{this._maybeRenderLoadingImage()}</SafeAreaView>);
    
      }
    
    
      _maybeRenderLoadingImage = () => {
        if (this.state.splashAnimationComplete) {
          return null;
        }
    
        return (
          <Animated.View
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              bottom: 0,
              right: 0,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: '#fff',
              opacity: this.state.splashAnimation.interpolate({
                inputRange: [0, 1],
                outputRange: [1, 0],
              }),
            }}>
            <Animated.Image
              source={require('./assets/splash.png')}
              style={{
                width: undefined,
                height: undefined,
                position: 'absolute',
                top: 0,
                left: 0,
                bottom: 0,
                right: 0,
                resizeMode: 'contain',
                transform: [
                  {
                    scale: this.state.splashAnimation.interpolate({
                      inputRange: [0, 1],
                      outputRange: [1, 4],
                    }),
                  },
                ],
              }}
              onLoadEnd={this._animateOut}
            />
          </Animated.View>
        );
      };
    
      _animateOut = () => {
        SplashScreen.hide();
        Animated.timing(this.state.splashAnimation, {
          toValue: 1,
          duration: 700,
          useNativeDriver: true,
        }).start(() => {
          this.setState({ splashAnimationComplete: true });
        });
      };
    
      _loadResourcesAsync = async () => {
        return Promise.all([
          Asset.loadAsync([
            require('./assets/splash.png'),
          ]),
        ]);
      };
    
      _handleLoadingError = error => {
        // In this case, you might want to report the error to your error
        // reporting service, for example Sentry
        console.warn(error);
      };
    
      _handleFinishLoading = () => {
        this.setState({ isLoadingComplete: true });
      };
    }


    const styles = StyleSheet.create({
      // …,
      safeArea: {
        flex: 1,
        backgroundColor: '#3b5998',
      },
      MainContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        padding: 20
      },
    
      TextStyle: {
        fontSize:20,
        textAlign: 'center',
        color:"#999"
      },
      mainheading1:{
          width:'100%',
      borderRadius:10,
      shadowColor: '#000',
      position:'relative',
      paddingTop:10,
      paddingRight:8,
      paddingLeft:8,
      },
      usernameTxt1:{
          fontSize:13,
          color:"#999",
          marginBottom:25,
      },
      offlineContainer: {
        backgroundColor: '#b52424',
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        width:"100%",
        position: 'absolute',
        bottom: 0
      },
      offlineText: { 
        color: '#fff'
      }
    })



/*
export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Open up App.js to start working on your app!</Text>
      </View>
    );
  }
}*/

AppRegistry.registerComponent('myapp', () => myapp);