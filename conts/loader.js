import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Modal,
    Text,
    ActivityIndicator
} from 'react-native';
import {
    ProgressDialog,
    ConfirmDialog
} from "react-native-simple-dialogs";

const Loader = props => {
    const {
        loading,
        ...attributes
        } = props;

    return (
        // <Modal
        //     transparent={true}
        //     animationType={'none'}
        //     visible={loading}
        //     onRequestClose={() => {console.log('close modal')}}>
        //     <View style={styles.modalBackground}>
        //         <View style={styles.activityIndicatorWrapper}>
        //             <ActivityIndicator
        //                 animating={loading} size="large" color="#000"/>
        //                 <Text style={{paddingLeft:10}}>Loading</Text>
        //         </View>
        //     </View>
        // </Modal>
        <ProgressDialog
                    activityIndicatorColor="#000"
                    activityIndicatorSize="large"
                    messageStyle={{color:'#333', fontSize:14}}
                    animationType="fade"
                    message="Loading"
                    dialogStyle={{backgroundColor:'transparent', elevation: 0, alignItems:'center'}}
                    contentStyle={{
                        backgroundColor:'#fff',
                        alignItems:'center',
                        justifyContent:'center',
                        width:200,
                        shadowColor: '#000',
                        shadowOffset: { width: 0, height: 2 },
                        shadowOpacity: 0.2,
                        shadowRadius: 5,
                        elevation: 3,
                        borderRadius:5}}
                    overlayStyle={{backgroundColor:'rgba(0, 0, 0, 0.5)'}}
                            visible={loading}

                        />
    )
}

const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: '#00000040'
    },
    activityIndicatorWrapper: {
        backgroundColor: '#FFFFFF',
        height: 80,
        width: 150,
        borderRadius: 10,
        padding:10,
        display: 'flex',
        flex:0.4,
        flexDirection:"row",
        alignItems: 'center',
        justifyContent: 'center'
    }
});

export default Loader;