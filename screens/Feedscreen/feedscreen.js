import React, { Component } from 'react';
import { StyleSheet, Text, View,ScrollView,RefreshControl,Dimensions,TouchableOpacity,ActivityIndicator,FlatList,ImageBackground,Share,Platform,AsyncStorage } from 'react-native';
import { Radio, CheckBox , Picker, Drawer, Body, Header, Title, Left , Right, Button,Badge,withBadge} from 'native-base';
import {styles} from './feedstyle';
import { Video } from 'expo';
import { Avatar,Image } from 'react-native-elements';
import Ripple from 'react-native-material-ripple';
import Icon from '@expo/vector-icons/MaterialCommunityIcons'
import ImageZoom from 'react-native-image-pan-zoom';
import Loader from '../../conts/loader'
import Storage from "../../conts/storage";
const { width, height } = Dimensions.get('window');



export class FeedScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            ids:'',
            checkBoxChecked: false,
            like: false,
            feedata:[],
            feedata1:'',
            loading:false,
            refresh:false,
            refresh1:false
            }
        // console.log('Getting code>>',this.state.isfrom)
        this.getpos()
        
    }
    componentDidMount () {
        this.getpos()
        this.setlocal()
    }
    setlocal= async(param)=>{
        try {
            await AsyncStorage.setItem('feededdata', JSON.stringify(param));
          } catch (error) {
            // Error saving data
            console.log('asyncstoreageerror1',error)
          }
          
          try {
            const myArray = await AsyncStorage.getItem('feededdata');
            if (myArray !== null) {
              // We have data!!
            //   console.log('storeinlocal>>',JSON.parse(myArray));
              this.setState({
                feedata1:JSON.parse(myArray)
              })
              console.log('store in local>>',this.state.feedata1)
            }
          } catch (error) {
            // Error retrieving data
            console.log('asyncstoreageerror2',error)
          }
    }
    // getpostlatest
    getpos=()=>{
        this.setState({refresh:true})
        const url ='http://172.104.54.149/LorealLeap/REST_API/v1/getPostsLatest'
        fetch(url,{
            method:'POST',
            body: JSON.stringify({
                "leapCode":"201800101671",
                "limit":15,
                "divisionArr":[
                    {"DivisionCode":"20",
                    "DivisionName":"L'ORÉAL PROFESSIONNEL "},
                    {"DivisionCode":"21","DivisionName":"MATRIX"},
                    {"DivisionCode":"22","DivisionName":"KERASTASE "},
                    {"DivisionCode":"23","DivisionName":"CHERYL'S COSMECEUTIC "} ,
                    {"DivisionCode":"24","DivisionName":"DECLEOR "}  ]
                })
        })
            .then((res) => res.json())
            .then((res) =>{
                this.setState({refresh:false})
                console.log('Getpostresponse',res.postData);
                    this.setState({
                        feedata:res.postData
                    })
                this.setlocal(res.postData)
            }).catch(error => {
            console.log('error',error)
        });
        
    }
    sharethisApp(shareurl) {
        this.setState({loading: true});
        setTimeout(() => {
            this.setState({loading: false});
        }, 100);
        Share.share({
          ...Platform.select({
            android: {
              message:
                shareurl,
                title:'Share'
            },
            ios: {
              message:
              shareurl,
                title:'Share'
            }
          })
        });
      }
  
    loop=()=>{
        let promo=[]
        for (let j = 0; j <15; j++) {
            promo.push(
                <View key={j+1}>
                    {/* looping */}
                    {/* Header */}
                  <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",padding:8}}>
                    <View style={{flexDirection:"row",alignItems:"center"}}>
                            <Image
                            style={{width:50,height:50,borderRadius:100}}
                            source={{uri:'https://i.imgur.com/8u3skSq.png'}}/>
                            <Text style={{paddingLeft:15,fontWeight:"500"}}>Example Name {j+1}</Text>
                    </View>
                    <Ripple transparent  activeOpacity={0.6} rippleContainerBorderRadius={100}>
                                <Icon size={30} color="#333" name="dots-vertical" />
                    </Ripple>
                  </View>
                  {/* Header */}
                  {/* images */}
                        <View style={{height: undefined,aspectRatio:1/1,width: "100%",backgroundColor:"#f1f1f1"}}>
                        {/* <ImageZoom cropWidth={Dimensions.get('window').width}
                       cropHeight={Dimensions.get('window').width} imageWidth={width}
                       imageHeight={width}> */}
                       <Ripple transparent  activeOpacity={0.6} rippleContainerBorderRadius={100} onPress={this.handleDoubleTap(j+1)}>
                            <Image
                            enableHorizontalBounce={true}
                            style={{height: "100%",resizeMode:'cover', width: "100%",}}
                            source={{uri:'https://i.imgur.com/jjqW5Ur.jpg'}}/>
                            {/* <Text style={{zIndex:1,position:'absolute',width:"100%",height:"100%",left:0,right:0,bottom:0,top:0}}>like</Text> */}
                            </Ripple>
                            {/* </ImageZoom> */}
                        </View>
                        {/* images */}
                        {/* sharenlikebutton */}
                        <View style={{flexDirection:"row",alignItems:"center",padding:8,flex:1}}>
                            <View style={{flexDirection:"row",alignItems:"center",flex:0.3,justifyContent:"space-between"}}>
                                <Ripple transparent  activeOpacity={0.6} rippleContainerBorderRadius={100} onPress={()=>this.like(j+1)}>
                                         {
                                             this.state.ids == (j+1)?
                                             <Icon size={30} color="#ee4955" name="heart" />
                                             :
                                             <Icon size={30} color="#333" name="heart-outline" />
                                         }   
                                </Ripple>
                                <Ripple transparent  activeOpacity={0.6} rippleContainerBorderRadius={100}>
                                            <Icon size={30} color="#333" name="comment-outline" />
                                </Ripple>
                                <Ripple transparent  activeOpacity={0.6} rippleContainerBorderRadius={100}>
                                            <Icon size={30} color="#333" name="share-outline" />
                                </Ripple>
                            </View>
                            <View style={{flexDirection:"row",alignItems:"center",flex:0.7,justifyContent:"flex-end"}}>
                                    <Ripple transparent  activeOpacity={0.6} rippleContainerBorderRadius={100}>
                                                <Icon size={30} color="#333" name="bookmark-outline" />
                                    </Ripple>
                            </View>
                        </View>
                        {/* sharenlikebutton */}
                        {/* likes */}
                    <View style={{flexDirection:"row",alignItems:"center",padding:8,paddingTop:0,flex:1}}>
                        <Text style={{fontWeight:'500'}}>{row.totalLikes} likes</Text>
                    </View>
                    {/* likes */}
                    {/* view comments */}
                    <View style={{flexDirection:"row",alignItems:"center",padding:8,paddingTop:0,flex:1}}>
                        <Text style={{fontWeight:'500',paddingRight:5}}>Example Name {j+1}</Text>
                        <Text style={{fontWeight:'100',flex:1}} numberOfLines={1} ellipsizeMode="tail">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam a tortor aliquam, dapibus orci at, vestibulum lorem. Maecenas eu felis ut urna ornare blandit. Vestibulum massa nisl, tincidunt in enim in, posuere finibus neque</Text>
                    </View>
                    <View style={{flexDirection:"row",alignItems:"center",padding:8,paddingTop:0,flex:1}}>
                        <Ripple transparent  activeOpacity={0.6} rippleColor="transparent">
                            <Text style={{fontWeight:'100',color:"#898989",flex:1}} >View All {j+1} Comments</Text>
                        </Ripple>
                    </View>
                    {/* view comments */}
                    {/* time */}
                    <View style={{flexDirection:"row",alignItems:"center",padding:8,paddingTop:0,flex:1}}>
                        <Ripple transparent  activeOpacity={0.6} rippleColor="transparent">
                            <Text style={{color:"#898989",flex:1,fontSize:10}} >8 hour ago</Text>
                        </Ripple>
                    </View>
                    {/* time */}
                    
                    {/* looped */}
                  </View>
            )
        }
        return promo
    }
    view=(param)=>{
        this.props.navigation.navigate("ViewfeedScreen",{'data':param});
    }
    _keyExtractor = (item, index) => item;
    renderItem=(row)=>{
        // console.log("Render Item>>>",row)
        return(
            <View style={{borderBottomWidth:1,borderColor:'#f1f1f1'}}>
                    {/* looping */}
                    {/* Header */}
                  <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",padding:8}}>
                    <View style={{flexDirection:"row",alignItems:"center"}}>
                        <Ripple transparent  activeOpacity={0.6} rippleContainerBorderRadius={100}>
                        <Image
                        style={{width:50,height:50,borderRadius:100}}
                        source={{uri:row.uploadedByPicture}}/>
                        </Ripple>
                        <Ripple transparent  activeOpacity={0.6} rippleColor="transparent">
                        <Text style={{paddingLeft:15,fontWeight:"500"}}>{row.uploadedByName}</Text>
                        </Ripple>
                    </View>
                    <Ripple transparent  activeOpacity={0.6} rippleContainerBorderRadius={100}>
                                <Icon size={30} color="#333" name="dots-vertical" />
                    </Ripple>
                  </View>
                  {/* Header */}
                  {/* images */}
                  <View style={{height: undefined,aspectRatio:1/1,width: "100%",backgroundColor:"#f1f1f1"}}>
                       <Ripple transparent  activeOpacity={0.6} onPress={()=>this.view(row)}>
                       {
                           row.is_image_video =='1'?
                           <ImageBackground source={{uri:row.postResource}}  style={{height: "100%",resizeMode:'cover', width: "100%",}} blurRadius={30}>
                            <Image
                                enableHorizontalBounce={true}
                                style={{height: "100%",resizeMode:'contain', width: "100%",}}
                                PlaceholderContent={<ActivityIndicator size={100} color="#dddddd"/>}
                                source={{uri:row.postResource}}/>
                            </ImageBackground>
                            :
                            <Video
                                source={{uri:row.postResource}}
                                rate={1.0} 
                                volume={1.0} 
                                isMuted={false} 
                                resizeMode="cover" 
                                shouldPlay
                                useNativeControls={false}  
                                isLooping                      
                                style={{height: "100%",width: "100%",}}
                                />
                       }
                        </Ripple>
                    </View>
                    {/* images */}
                    {/* sharenlikebutton */}
                    <View style={{flexDirection:"row",alignItems:"center",padding:8,flex:1}}>
                            <View style={{flexDirection:"row",alignItems:"center",flex:0.3,justifyContent:"space-between"}}>
                                <Ripple transparent  activeOpacity={0.6} rippleContainerBorderRadius={100} >
                                         {
                                             row.isLiked != '0'?
                                             <Icon size={30} color="#ee4955" name="heart" />
                                             :
                                             <Icon size={30} color="#333" name="heart-outline" />
                                         }   
                                </Ripple>
                                <Ripple transparent  activeOpacity={0.6} rippleContainerBorderRadius={100}>
                                            <Icon size={30} color="#333" name="comment-outline" />
                                </Ripple>
                                <Ripple transparent  activeOpacity={0.6} rippleContainerBorderRadius={100} onPress={()=>this.sharethisApp(row.postingUrl)}>
                                            <Icon size={30} color="#333" name="share-outline" />
                                </Ripple>
                            </View>
                            <View style={{flexDirection:"row",alignItems:"center",flex:0.7,justifyContent:"flex-end"}}>
                                    <Ripple transparent  activeOpacity={0.6} rippleContainerBorderRadius={100}>
                                                <Icon size={30} color="#333" name="bookmark-outline" />
                                    </Ripple>
                            </View>
                        </View>
                        {/* sharenlikebutton */}
                        {/* likes */}
                    <View style={{flexDirection:"row",alignItems:"center",padding:8,paddingTop:0,flex:1}}>
                        <Text style={{fontWeight:'500'}}>{row.totalLikes} likes</Text>
                    </View>
                    {/* likes */}
                    {row.postTitle.length?
                        <View style={{flexDirection:"row",alignItems:"center",padding:5,paddingTop:0,flex:1}}>
                        <Text style={{fontWeight:'500',paddingRight:5}}>{row.uploadedByName}</Text>
                        <Text style={{fontWeight:'100',flex:1,color:"#333"}} numberOfLines={1} ellipsizeMode="tail">{row.postTitle}</Text>
                    </View>
                        :null}
                    {/* time */}
                    <View style={{flexDirection:"row",alignItems:"center",padding:8,paddingTop:0,flex:1}}>
                        <Ripple transparent  activeOpacity={0.6} rippleColor="transparent">
                            <Text style={{color:"#898989",flex:1,fontSize:10}} >{row.elapsedTime}</Text>
                        </Ripple>
                    </View>
                    {/* time */}
            </View>
        )
    }
    onscroll=(param)=>{
// console.log('touchdown>>',param)
this.renderItem1(this.state.feedata)
    }
    renderItem1=(row)=>{
        const post=row.postId
        if(post-1){
            console.log('new data>>',row)
        }
        
    }
    _onrefresh=()=>{
        this.getpos()
        this.renderItem1(this.state.feedata)
        this.setState({
            refresh:false
        })
    }
    enableSomeButton=()=>{
        console.log('pull up')
        this.setState({
            refresh1:false
        })
    }
    render() {
        const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
            const paddingToBottom = 20;
            return layoutMeasurement.height + contentOffset.y >=
              contentSize.height - paddingToBottom;
          };
        return (
            <View style={styles.mainContainer}>
            <View style={styles.header}>
                <Header style={{height:'100%', backgroundColor:'#f9f9f9', elevation:0}}>
                    <Left>
                        {/* <Ripple transparent  activeOpacity={0.6} rippleContainerBorderRadius={100}>
                            <Icon size={30} color="black" name="arrow-left" />
                        </Ripple> */}
                    </Left>
                    <Body>
                    <Title style={styles.homeTxt}>Learn n Connect</Title>
                    </Body>
                    <Right>
                        <Button transparent>
                            {/*<Image source={require('../../assets/icon/notification.png')} style={styles.menuBtnRight} />*/}
                        </Button>
                    </Right>
                </Header>
            </View>
            <View style={styles.peoplecontainer}>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center',width:"100%" }}>
                <ScrollView
                showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}
                bounces={false}
                // onScrollEndDrag={()=>this.onscroll(this.state.feedata)}
                    refreshControl={
                    <RefreshControl
                        refreshing={this.state.refresh}
                        onRefresh={this._onrefresh}
                        tintColor={"black"}
                    />
                    }
                    onScroll={({nativeEvent}) => {
                        if (isCloseToBottom(nativeEvent)) {
                            <RefreshControl
                                refreshing={this.state.refresh1}
                                onRefresh={this.enableSomeButton}
                                tintColor={"black"}
                            />
                          
                        }
                      }}
                      scrollEventThrottle={400}
                >
                 {this.state.feedata.length > 0 ?
                    this.state.feedata.map((row, k) => <View key={k} style={{ flex: 1 }}>
                       {this.renderItem(row)}
                    </View>)
                    : this.state.feedata1.length > 0?
                    this.state.feedata1.map((row, k) => <View key={k} style={{ flex: 1 }}>
                        {this.renderItem(row)}
                    </View>):
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                        <ActivityIndicator size={50} color="#dddddd" />
                    </View>
                }
                  </ScrollView>
                </View>
                <Loader loading={this.state.loading}/>
            </View>
        </View>
        )
    }
}

