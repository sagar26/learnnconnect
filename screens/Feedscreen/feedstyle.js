import {Platform, StatusBar, StyleSheet } from 'react-native'


const styles = StyleSheet.create({
    mainContainer:{
        flex:1,
        backgroundColor:'#fff',
        height:'100%',
        width:'100%',
    },
    homecontainer:{
        flex:0.9,
        height:'100%',
        width:'100%',
        alignItems:'center',
        justifyContent:'center',
    },
    header:{
        width:"100%",
        flex:0.1,
        backgroundColor: "#f9f9f9",
        display:'flex',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 5,
        elevation: 3,
        ...Platform.select({
            ios:{
                marginTop:20,

            },
            android:{
                paddingTop: StatusBar.currentHeight,
                backgroundColor:'#f9f9f9'
            }
        }),
    },

    menuView:{

        flex:1,
        justifyContent:'center',
        alignItems:'center',
        textAlign:'center'

    },

    menuBtn:{
        width:20,
        height:20,
        resizeMode:'contain'
    },
    menuBtnRight:{
        width:20,
        height:20,
        resizeMode:'contain'
    },
    homeTxt:{
        color:'#000' ,
        fontSize:17,
    },
    addpeopleImage:{
        flex:0.20,
        height:'100%',
        width:'100%',
        resizeMode:'contain'
    },
    titleText:{
      fontSize:16,
      color:'#222',
      letterSpacing:2,
        marginBottom:5
    },
    subtitleText:{
      fontSize:14,
      color:'#999'
    },
    addBtn:{
        width:'30%',
        flexDirection: 'row',
        flex:0.06,
        borderRadius:25,
        textAlign:'center' ,
        justifyContent:'center',
        alignItems:'center',
        marginBottom:15,
        backgroundColor:'#F50057',
        marginTop:15,
    },
    addTxt:{
        color:'#fff',
        fontSize:12,
        textTransform:'uppercase',
        letterSpacing:1
    },
    homesec:{
        flexDirection:'row',
        alignItems:'center',
        paddingTop:10,
        paddingBottom:10,
        paddingRight:10,
        width:'100%',
        flex:1,
        backgroundColor:'#fff',
        position:'relative',
        borderBottomWidth:1,
        borderColor:'#f1f1f1',
    },
    peoplecontainer:{
        flex:0.9,
        height:'100%',
        width:'100%',
        paddingTop:0,
        alignItems:'center',
    },
    proImage:{
        height:55,
        width:55,
        resizeMode:'cover',
        borderRadius:100

    },
    promaincont:{
        flex:1,
        marginRight:0,
        alignItems:'center',
        justifyContent:'center'
    },
    rightIcon:{
        flex:0.25,
        height:'100%',
        width:'100%',
        resizeMode:'contain',
        opacity:0.2
    },
    titleSec:{
        flex:3,
        flexDirection:'column',
        marginLeft:10
    },
    username:{
        fontSize:16,
        color:'#333',
        marginBottom:3
    },
    relationTitle:{
        fontSize:12,
        color:'#999',
    },
    addfab:{
        height:60,
        width:60,
        position:'absolute',
        bottom:25,
        right:25,
        borderRadius:100,
        backgroundColor:'#F50057',
        alignItems:'center',
        justifyContent:'center',
    },
    addfabimg:{
      width:20,
      height:20,
        borderRadius:100
    },
    badgStyle:{
        flex:1.5,
        alignItems:'center',
        justifyContent:'center'

    },
    badgText:{
       color:'#fff',
       fontSize:10,
        letterSpacing:1,
        textTransform:'uppercase',
        padding:5,
        paddingRight:15,
        paddingLeft:15,
        backgroundColor:'#4CAF50',
        borderRadius:20,

    },
});

export { styles }