import React, { Component } from 'react';
import { StyleSheet, Text, View,ScrollView,RefreshControl,Dimensions,TouchableOpacity,ActivityIndicator,FlatList,ImageBackground,Share,Platform,AsyncStorage } from 'react-native';
import { Radio, CheckBox , Picker, Drawer, Body, Header, Title, Left , Right, Button,Badge,withBadge} from 'native-base';
import {styles} from './viewfeedstyle';
import { Video } from 'expo';
import { Avatar,Image } from 'react-native-elements';
import Ripple from 'react-native-material-ripple';
import Icon from '@expo/vector-icons/MaterialCommunityIcons'
import ImageZoom from 'react-native-image-pan-zoom';
import Loader from '../../conts/loader'
import Storage from "../../conts/storage";
const { width, height } = Dimensions.get('window');



export class ViewfeedScreen extends React.Component {
    constructor(props) {
        super(props);
        const isfrom = this.props.navigation.getParam('data');
        this.state = {
            data:isfrom,
            loading:false,
        }
        console.log("Data >>",this.state.data)
        
    }

    sharethisApp(shareurl) {
        this.setState({loading: true});
        setTimeout(() => {
            this.setState({loading: false});
        }, 100);
        Share.share({
          ...Platform.select({
            android: {
              message:
                shareurl,
                title:'Share'
            },
            ios: {
              message:
              shareurl,
                title:'Share'
            }
          })
        });
      }
    render() {
        const row =this.state.data
        return (
            <View style={styles.mainContainer}>
                <View style={styles.header}>
                    <Ripple transparent activeOpacity={0.6} rippleContainerBorderRadius={100} onPress={()=>this.props.navigation.navigate("FeedScreen")}>
                        <Icon size={30} color="black" name="arrow-left" />
                    </Ripple>
                    <View style={{flexDirection:"row",alignItems:"center",paddingLeft:5}}>
                        <Image
                        style={{width:50,height:50,borderRadius:100}}
                        source={{uri:row.uploadedByPicture}}/>
                        <Text style={{paddingLeft:15,fontWeight:"500"}}>{row.uploadedByName}</Text>
                    </View>
                </View>
            <View style={styles.peoplecontainer}>
                <View style={{ height: Dimensions.get('window').height,width:Dimensions.get('window').width}}>
                {/* <ScrollView
                showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}
                bounces={false}
                style={{ flex: 1 }}
                > */}
                  {/* images */}
                  {/* <View style={{height: Dimensions.get('window').height,width:Dimensions.get('window').width,backgroundColor:"#f1f1f1"}}>
                       {
                           row.is_image_video =='1'?
                           <ImageBackground source={{uri:row.postResource}}  style={{height: Dimensions.get('window').height,height: Dimensions.get('window').width,resizeMode:'cover'}} blurRadius={30}>
                           <ImageZoom cropWidth={Dimensions.get('window').width}
                                cropHeight={Dimensions.get('window').width}
                                imageWidth={Dimensions.get('window').width}
                                imageHeight={Dimensions.get('window').width}>
                            <Image
                                enableHorizontalBounce={true}
                                style={{height: Dimensions.get('window').height,height: Dimensions.get('window').width,resizeMode:'contain'}}
                                PlaceholderContent={<ActivityIndicator size={100} color="#dddddd"/>}
                                source={{uri:row.postResource}}/>
                                </ImageZoom>
                            </ImageBackground>
                            :
                            <Video
                                source={{uri:row.postResource}}
                                rate={1.0} 
                                volume={1.0} 
                                isMuted={false} 
                                resizeMode="cover" 
                                shouldPlay
                                useNativeControls={false}  
                                isLooping                      
                                style={{height: "100%",width: "100%",}}
                                />
                       }
                    </View> */}
                    <ImageBackground source={{uri:row.postResource}}  style={{height: Dimensions.get('screen').height,height: Dimensions.get('screen').width,resizeMode:'cover'}} blurRadius={30}>
                        <ImageZoom cropWidth={Dimensions.get('screen').width}
                            cropHeight={Dimensions.get('screen').width}
                            imageWidth={Dimensions.get('screen').width}
                            imageHeight={Dimensions.get('screen').width}>
                            <Image
                            enableHorizontalBounce={true}
                            style={{height: Dimensions.get('screen').height,height: Dimensions.get('screen').width,resizeMode:'contain'}}
                            PlaceholderContent={<ActivityIndicator size={100} color="#dddddd"/>}
                            source={{uri:row.postResource}}/>
                        </ImageZoom>
                    </ImageBackground>
                    {/* images */}
                    {/* {row.postTitle.length?
                        <View style={{flexDirection:"column",padding:5,paddingTop:0,flex:1}}>
                        <Text style={{fontWeight:'500',paddingRight:5,}}>{row.uploadedByName}</Text>
                            <Text style={{fontWeight:'100',flex:1,color:"#333",paddingTop:5}}>{row.postTitle}</Text>
                        </View>
                        :null} */}
                    {/* time */}
                    {/* <View style={{flexDirection:"row",alignItems:"center",padding:8,paddingTop:0,flex:1}}>
                        <Ripple transparent  activeOpacity={0.6} rippleColor="transparent">
                            <Text style={{color:"#898989",flex:1,fontSize:10}} >{row.elapsedTime}</Text>
                        </Ripple>
                    </View> */}
                    {/* time */}
                  {/* </ScrollView> */}
                </View>
            </View>
            <Loader loading={this.state.loading}/>
        </View>
        )
    }
}

